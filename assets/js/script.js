console.log("Hello World");


let trainer = Object();
	trainer.name = "Aaron";
	trainer.age = 32;
	trainer.hometown = "Cavite";
	trainer.greeting = function(){
		console.log("Pokemon trainer")
	}
console.log(trainer);
console.log(trainer.name);
console.log(trainer.age);
console.log(trainer.greeting());



 function Pokemon(name, level, element){
	this.name = name;
	this.level = level;
	this.attack = level * 1.2;
	this.health = 115 + (1.2 * level);
	this.element = element;
	this.tackle = function(target){
		console.log(`${this.name} tackled ${target.name}`);
		target.health -= 10;
		console.log(`${target.name} health is now ${target.health}`)
	}
};

let Charmander = new Pokemon("Charmander", 20, "fire");
console.log(Charmander);

let Squirtle = new Pokemon("Squirtle", 15, "water");
console.log(Squirtle);

let Snorlax = new Pokemon("Snorlax", 12, "normal");
console.log(Snorlax);

let Magikarp = new Pokemon("Magikarp", 18, "aqua");
console.log(Magikarp);

Charmander.tackle(Squirtle);
Squirtle.tackle(Snorlax);
Magikarp.tackle(Charmander);

